import { loadRemoteModule } from '@angular-architects/module-federation';
import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';

const URL = 'http://localhost:3000/remoteEntry.js';
const SPOZ = 'http://localhost:3003/remoteEntry.js';

export const APP_ROUTES: Routes = [
    {
      path: '',
      component: HomeComponent,
      pathMatch: 'full'
    },
    {
      path: 'flights',
      loadChildren: () => loadRemoteModule({
          type: 'module',
          remoteEntry: URL,
          exposedModule: './Module'
        })
        .then(m => m.FlightsModule)
    },
  {
    path: 'food',
    loadChildren: () => loadRemoteModule({
      type: 'module',
      remoteEntry: SPOZ,
      exposedModule: './Module'
    })
      .then((m) => m.FoodModule)
  },
    {
      path: '**',
      component: NotFoundComponent
    }

];

